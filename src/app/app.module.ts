import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';

import { PerformanceUiModule } from './module/performance-ui/performance-ui.module';
import { MailService } from './service/mail-service';
import { NewsService } from './service/news-service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PerformanceUiModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule
  ],
  providers: [
    MailService,
    NewsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
