import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';

import { MailService } from './service/mail-service';
import { NewsService } from './service/news-service';

import { of } from 'rxjs';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { PerformanceUiModule } from './module/performance-ui/performance-ui.module';

describe('AppComponent', () => {
  
  beforeEach(async(() => {
    const mockMailService = jasmine.createSpyObj('MailService', ['getMesssages']);
    const mockNewsService = jasmine.createSpyObj('NewsService', ['getMesssages']);

    mockMailService.getMesssages = jasmine.createSpy().and.returnValue(of({}));
    mockNewsService.getMesssages = jasmine.createSpy().and.returnValue(of({}));

    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        PerformanceUiModule
      ],
      providers: [
        MailService, { provide: MailService, useValue: mockMailService },
        NewsService, { provide: NewsService, useValue: mockNewsService }
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should have title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Infinite List');
  });
});
