import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';

import { MessageService } from '../service/message-service';
import { Message } from '../service/message';

import { Observable } from 'rxjs';
import { tap, flatMap, distinct } from 'rxjs/operators';

@Component({
  selector: 'virtual-list',
  templateUrl: './virtual-list.component.html',
  styleUrls: ['./virtual-list.component.css']
})

export class VirtualList implements OnInit {

  @Input() sourceService: MessageService;

  @ViewChild(CdkVirtualScrollViewport)
  viewport: CdkVirtualScrollViewport;

  filterForName: string;

  avatarStyle = {
    borderRadius: "50%",
  };

  messages: Message[];
  nameList: string[] = [];

  ngOnInit(){
    if (this.sourceService) {
      this.getNextBatch(0);
    } else {
      console.error(`Required sourceService property is missing`);
    }
  }

  getNextBatch(lastVisibleId: number, filterForName?: string): void {
    console.log(`Pull next batch beyond the last visible message id ${lastVisibleId}, filterForName ${filterForName}`);
    this.sourceService.getMesssages(lastVisibleId, filterForName).pipe(
      tap( msgArray => {
        if (msgArray && msgArray.length > 0) {
          let message: Message = msgArray[0];
          console.log(`Get next batch beyond id ${lastVisibleId}. Received the first message id ${message.id}, subject "${message.subject}".`);

          if (!this.messages) {
            this.messages = new Array<Message>();
          }
          this.messages = [...this.messages, ...msgArray];
        }
      }),
      flatMap(msgArray => Object.values(msgArray)),
      distinct((msg: Message) => msg.from.name),
      tap(msg => {
        if (!this.nameList.includes(msg.from.name)) {
          console.log(`Found a new name, ${msg.from.name}`);
          this.nameList.push(msg.from.name)
        };
      })
    ).subscribe();
  }

  nextBatchIfNeeded(e, lastVisibleId: number): void {
    const end = this.viewport.getRenderedRange().end;
    const total = this.viewport.getDataLength();

    console.log(`The last visible item index of ${end} >= ${total} of available index ?`);

    if (end == total) {
      this.getNextBatch(lastVisibleId, this.filterForName);
    }
  }

  reload(): void {
    console.log(`Reload with filtering for name, ${this.filterForName}`);

    if (this.viewport) {
      this.viewport.scrollToOffset(0);
    }
    this.messages = undefined;
    this.nameList = [];
    this.getNextBatch(0, this.filterForName);
  }
}
