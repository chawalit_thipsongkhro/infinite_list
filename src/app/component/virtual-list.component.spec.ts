import { TestBed, async } from '@angular/core/testing';
import { VirtualList } from './virtual-list.component';

import { of } from 'rxjs';

import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AvatarModule } from "ngx-avatar";

describe('VirtualList', () => {
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        VirtualList
      ],
      imports: [
        ScrollDispatchModule,
        FormsModule,
        MatSelectModule,
        BrowserAnimationsModule,
        AvatarModule
      ],
    }).compileComponents();
  }));

  it('should create the component', () => {
    const mockMailService = jasmine.createSpyObj('MailService', ['getMesssages']);

    mockMailService.getMesssages = jasmine.createSpy().and.returnValue(of([
      {
          "id" : 5000,
          "from" : {
              "name" : "Now TV",
              "email" : "nowtv@test.com"
          },
          "subject" : "Grab another Pass, you need to be watching this...",
          "body" : "Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting the likes of Thandie Newton, James Marsden and Jeffrey Wright."
      }
    ]));

    const fixture = TestBed.createComponent(VirtualList);
    const component = fixture.debugElement.componentInstance;   
    //component.sourceService = mockMailService;

    expect(component).toBeTruthy();
  });

});
