import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VirtualList } from 'src/app/component/virtual-list.component';
import { AvatarModule } from "ngx-avatar";

@NgModule({
  declarations: [VirtualList],
  imports: [
    CommonModule,
    FormsModule,
    MatSelectModule,
    ScrollingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AvatarModule
  ],
  exports: [VirtualList]
})
export class PerformanceUiModule { }
