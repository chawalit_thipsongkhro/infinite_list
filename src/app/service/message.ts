export interface Message {
    id: number,
    from: {
        name: string,
        email: string,
    },
    subject: string
    body: string
}
