import { Observable } from 'rxjs';
import { Message } from './message';

export interface MessageService {
    getMesssages(lastVisibleId: number, filterForName?: string): Observable<Message[]>;
}
