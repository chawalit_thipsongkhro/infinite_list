import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Message } from './message';
import { MessageService } from './message-service';

const limit: number = 20;

@Injectable()
export class MailService implements MessageService {

    public apiBase: string = "http://localhost:4200/assets/mails.json";
    // when using json-server, public apiBase: string = "http://localhost:8081/mails";

    constructor(private _http: HttpClient) { }            

    getMesssages(lastVisibleId: number, filterForName?: string): Observable<Message[]> {
        let filterForNameParam: string = filterForName ? filterForName : "";
        let url: string = `${this.apiBase}?_limit=${limit}`;

        if (lastVisibleId > 0) {
            url += `&id_lte=${lastVisibleId}&id_ne=${lastVisibleId}`;
            // when json-server supports "lt" operator it will be, url += `&id_lt=${lastVisibleId}`;
        }
        if (filterForNameParam) {
            url += `&from.name=${filterForNameParam}`;
        }

        console.log(`MailService GET ${url}`);
        return this._http.get<Message[]>(url);
    }

}
