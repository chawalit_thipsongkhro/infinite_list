import { Component, OnInit } from '@angular/core';

import { MailService } from './service/mail-service';
import { NewsService } from './service/news-service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  mailBaseApiField: FormControl = new FormControl('test1');
  newsBaseApiField: FormControl = new FormControl('test2');

  constructor(
    public mailService: MailService,
    public newsService: NewsService
  ) { }

  ngOnInit() {
    debugger;
    this.mailBaseApiField.setValue(this.mailService.apiBase)
    this.newsBaseApiField.setValue(this.newsService.apiBase)
  }

  setMailServiceApiBase() {
    this.mailService.apiBase = this.mailBaseApiField.value;
  }

  setNewsServiceApiBase() {
    this.newsService.apiBase = this.newsBaseApiField.value;
  }

}
