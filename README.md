# infinite_list

Angular 7 Virtual List component using view port for lesser DOM rendering, and fetching more data in batch from API URL only when scroll down to the last visible one.

## Required Software

- Google Chrome as browser
- nodejs
- npm

## Running application in development box

Run these commands in terminal:

```
$ npm install
$ npm start
```

Wait until "Compiled successfully." appears on terminal.<br>
Open browser. Navigate to http://localhost:4200.<br>
Down below the page is developer playground for testing switching message service API, which will effect next batch pull.<br>
Data of each batch is duplicate as currently they are mock assets files, mails.js and news.js, instead of real message API URI of the message servers.<br>
The real message server is supposed to do querying similar to this SQL, when ${lastVisibleId} is request parameter from mail-service.ts and news-service.ts.<br>

```
SELECT * FROM mails WHERE name='${name}' AND id < ${lastVisibleId} ORDER BY id DESC
SELECT * FROM news  WHERE name='${name}' AND id < ${lastVisibleId} ORDER BY id DESC
```

## How to test with realistic RESTful server

Install json-server once

```
npm install -g json-server
```
Modify src/assets/messages.json with mails and news data.
From source code folder, run this command before running the application. Port 8081 can be any free port.

```
json-server --watch src/assets/messages.json --port 8081
```

Change `Mail service API` and `News service API` to these values respectively. Click `Update` then change Name `Filter` to reload data.

```
http://localhost:8081/mails
http://localhost:8081/news
```

## Running unit tests

Run this command in terminal:

```
$ npm test
```

## Running end-to-end tests

Run this command in terminal:

```
$ npm run e2e
```
